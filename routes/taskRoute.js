// Contains all the endpoints for our application 
// We separate the routes such that "index.js" only contains the information on the server

const express = require('express');
const router = express.Router();

const taskController = require('../controllers/taskController')

// Routes
// Route to get all the tasks 

router.get('/', (request, response) => {

	taskController.getAllTasks().then(resultFromController => response.send(resultFromController));
});

router.post('/', (request, response) => {

	console.log(request.body);
	taskController.createTask(request.body).then(resultFromController => response.send(resultFromController));
})

router.delete('/:id', (request, response) => {
	console.log(request.params)

	taskController.deleteTask(request.params.id).then(resultFromController => response.send(resultFromController));
})

router.put('/:id', (request, response) => {

	taskController.updateTask(request.params.id, request.body). then(resultFromController => response.send(resultFromController));
})

// Activity
// Create a route for getting a specific task.
router.get('/:id', (request, response) => {
	taskController.getSpecificTask(request.params.id).then(resultFromController => response.send(resultFromController));
})

//Create a route for changing the status of a task to complete.
router.put('/:id/complete', (request, response) => {
	taskController.updateStatus(request.params.id, request.body.status).then(resultFromController => response.send(resultFromController));
})

// Use "module.exports" to export the router object to use in the "index.js"
module.exports = router;

