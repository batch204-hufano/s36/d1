// Controllers contain the function and business logic of our Express JS application 

// Allow us to use the contents of the "Task.js" file in the models folder 
const Task = require('../models/Task');

// Controller function for getting all the tasks

module.exports.getAllTasks = () => {

	return Task.find({}).then(result => {

		return result
	})
}


module.exports.createTask = (requestBody) => {

	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {

		if(error) {
			console.log(error)
			return false
		} else {
			return task
		}
	})
}

module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {

		if(err) {
			console.log(err);
			return false
		} else {

			return removedTask
		}
	})
}

module.exports.updateTask = (taskId, newContent) => {

	// console.log(taskId) // Result : 633d856d98a5411370158ead
	// console.log(newContent) // Result : Wake-up

	return Task.findById(taskId).then((result, error) => {

		if (error) {
			console.log(error)
			return false
		}

		result.name = newContent.name;

		return result.save().then((updateTask, saveErr) => {
			if(saveErr) {
				console.log(saveErr)
				return false
			} else {
				return updateTask
			}
		})
	})
}

// Activity 
// Create a controller function for retrieving a specific task.
module.exports.getSpecificTask = (taskId) => {

	return Task.findById(taskId).then((task, error) => {
		
		if(error) {

			console.log(error);
			return false
		} else {

			return task
		}
	})
}

// Create a controller function for changing the status of a task to complete.

module.exports.updateStatus = (taskId, newContent) => {

	return Task.findById(taskId).then((result, error) => {

		if(error) {
			console.log(error)
			return false
		}


		result.status = newContent;

		return result.save().then((updateTask, error) => {
			if(error) {
				console.log(error)
				return false
			} else {

				return updateTask
			}
		})
	})
}